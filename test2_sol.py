# implement an iterator for number
# should have an iter that returns an iterator as well as
# a functionality that returns the sum of all digits as it iterates

class ld_iter:
  def __init__(self, l):
    self.l = l
    self.i = iter(l)
  
  def __iter__(self):
    return self

  def __next__(self):
    i = next(self.i)
    sum = 0
    while(i > 0):
      val = i % 10
      i = i // 10
      sum += (val)
    return sum + i


a = [123, 222, 655, 444, 323, 444]
p = iter(ld_iter(a))
assert hasattr(p, "__next__")
print(next(p), end = ' ')
try:
  while True:
    print(next(p), end = ' ')
except StopIteration:
  pass
# 6 6 16 12 8 12

print()
a = [0, 11, 12, 100]
p = iter(ld_iter(a))
try:
  while True:
    print(next(p), end = ' ')
except StopIteration:
  pass
# 0 2 3 1

a = [111, 222, 333, 444]
p = list(iter(ld_iter(a)))
print()
print(p)
assert(p == [3, 6, 9, 12])
# [3, 6, 9, 12]
