# replace all values in list with x^x in a LAZY manner
# debug information should read "calculating..." at top.
# use both class and function decorators to achieve 
# this debug statement

# USE CLASS AND DEF IMPLEMENTATION

# FUNCTION IMPLEMENTATION

# def debug_function(f):
#   def printcalculate(n):
#     print("calculating...")
#     return (v**v for v in n)
#   return printcalculate

# @debug_function 
# def f(x):
#   return debug_function(x)

# CLASS IMPLEMENTATION

class debug_class:
  def __init__(self, f):
    self.f = f
  
  def __call__(self, a):
    print("calculating...")
    m = self.f(a)
    return m


@debug_class
def f(x):
  return (v**v for v in x)




#test1
a = [1, 2, 3, 4]
result = (list(f(a)))
print(result)
assert(result == [1, 4, 27, 256])

# calculating...
# finished...
# [1, 4, 27, 256]

