# replace all values in list with x^x in a LAZY manner
# debug information should read "calculating..." at top.
# use both class and function decorators to achieve 
# this debug statement


# FUNCTION IMPLEMENTATION


# CLASS IMPLEMENTATION



@debug_class


#test1
a = [1, 2, 3, 4]
result = (list(f(a)))
print(result)
assert(result == [1, 4, 27, 256])

# calculating...
# finished...
# [1, 4, 27, 256]

